Legacy carefully selects its industry partners and its projects. We focus on performing upfront due diligence of potential oil and gas projects so that our investing partners have a greater probability of success in the oil and gas industry.

Website: https://legacyexploration.com/
